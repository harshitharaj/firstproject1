package Arraylist;

import java.util.ArrayList;
import java.util.Iterator;

public class Arraylistlooping {

	public static void main(String[] args) {
	
		ArrayList<Integer>arrylist= new ArrayList<Integer>();
		arrylist.add(14);
		arrylist.add(6);
		arrylist.add(30);
		System.out.println("using the for loop");
		for(int count=0;count<arrylist.size();count++)
		{
			System.out.println(arrylist.get(count));
		}
			
		System.out.println("using while loop");
		int  count=0;
		while(arrylist.size()>count)
		{
			System.out.println(arrylist.get(count));
			count++;
		}
		
		System.out.println("using iterator");
		  Iterator<Integer> ite = arrylist.iterator();
		while(ite.hasNext())
		{
			System.out.println(ite.next());
		}
		
		System.out.println("using advance loop");
		for (Integer number:arrylist)
		{
			System.out.println(number);
		}
		
		

	}

}
