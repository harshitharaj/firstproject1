package fuelassignment;
import java.util.Objects;
import java.util.Scanner;

public class carfuel {

	public static void main(String[] args)
	{
		int petrolcost=115;
		
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter the type of vehicle bike or car : ");
        String vehicle = sc.nextLine();
        
        System.out.print("Enter the amount of petrol: ");
        int amt = sc.nextInt();
        
        result(amt,vehicle);
    }
	
    public static void result(int amount, String vehicle)
    {
        int litres = amount/115;
        int distance=0;
        if(vehicle.equalsIgnoreCase("car"))
        {
            distance = 8*litres;
        }
        else if(vehicle.equalsIgnoreCase("bike"))
        {
            distance = 20*litres;
        }
        else
        {
            System.out.println("Enter valid vehicle type - (bike/car)");
            return;
         }
        System.out.println("Litres of petrol the customer gets : "+litres);
        System.out.println("Total distance customer can travel : "+distance);
        }
    }
        
	