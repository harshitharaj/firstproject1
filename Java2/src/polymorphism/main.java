package polymorphism;

public class main {

	public static void main(String[] args) {
	birds b = new birds();
	parrot p = new parrot();
	pigeon pi = new pigeon();
	b.features();
	pi.features();
	p.features();
	}

}
