package MavanAssig;

public class Jsonjava {
	
	    private String name;
	    private int age;
	    private String gender;
	    private String city;
	    private String country;
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name=name;
	    }
	    public int getage() {
	        return age;
	    }
	    public void setage(int age) {
	        this.age=age;
	    }
	    public String gender() {
	        return gender;
	    }
	    public void setgender(String gender) {
	        this.gender=gender;
	    }
	    public String city() {
	        return city;
	    }
	    public void setcity(String city) {
	        this.city=city;
	    }
	    public String country() {
	        return country;
	    }
	    public void setcountry(String country) {
	        this.country=country;
	    }
	    public String toString() {
	        return "Jsonjava[name=" +name+ ",age=" +age+",gender="+gender+",city="+city+"]";
	
		
	}
}


