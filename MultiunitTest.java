package Multiunit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MultiunitTest {

	@BeforeAll
    static void setUpBeforeClass()  {
		Multiplicationunit sb=new  Multiplicationunit();
		 assertEquals(2,sb.subtraction1(8, 4, 2));
	}

	@AfterAll
	static void tearDownAfterClass()
	{
		Multiplicationunit addition=new  Multiplicationunit();
		 assertEquals(10,addition.addition1(3, 3, 3, 1));
	}

	@BeforeEach
	void setUp() {
		Multiplicationunit division=new  Multiplicationunit();
		 assertEquals(1.5,division.division1(9, 6));
	}

	@AfterEach
	void tearDown()  {
		Multiplicationunit multiplication=new  Multiplicationunit();
		 assertEquals(36,multiplication.Multipli(6, 6));
	}

	@Test
	void test() {
		Multiplicationunit average=new  Multiplicationunit();
		 assertEquals(5,average.avgerage(6, 6, 3));
	}

}
