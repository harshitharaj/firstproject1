package Secondlargestnumber;

import java.util.Scanner;

public class SecondLargNum {

	public static void main(String[] args)
	{
		Scanner sca=new Scanner(System.in);
        int n;     //Declare array size
        System.out.println("Enter the array size");
        n=sca.nextInt() ;  //Initialize array size
        
        int arry[]=new int[n];   //Declare array
       System.out.println("Enter the array");  
       for(int i=0;i<n;i++)     //Initialize array
       {
           arry[i]=sca.nextInt();
       }
              
       for(int i=0;i<n;i++) //Use to hold the element
       {
           for(int j=i+1;j<n;j++)//Use to compare with the rest of the elements
           {
               if(arry[i]<arry[j])  //Check and swap
               {
                   int temp=arry[i];
                   arry[i]=arry[j];
                   arry[j]=temp;
               }
           }
       }
       
       System.out.println("Second Largest element is "+arry[1]);   //Display second largest element.
		
		

	}

}
